<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('user', 'UserController');
Route::resource('role', 'RoleController');
Route::resource('profile', 'ProfileController');
Route::resource('result', 'ResultController');
// Route::resource('exam', 'ExamController');
// Route::resource('group', 'GroupController');
// Route::resource('question', 'QuestionController');
Route::resource('choice', 'ChoiceController');
