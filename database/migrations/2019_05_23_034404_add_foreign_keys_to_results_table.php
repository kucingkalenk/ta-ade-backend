<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('results', function(Blueprint $table)
		{
			$table->foreign('user', 'results_fk0')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('exam', 'results_fk1')->references('id')->on('exams')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('question_bank', 'results_fk2')->references('id')->on('bank')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('answer', 'results_fk3')->references('id')->on('choices')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('results', function(Blueprint $table)
		{
			$table->dropForeign('results_fk0');
			$table->dropForeign('results_fk1');
			$table->dropForeign('results_fk2');
			$table->dropForeign('results_fk3');
		});
	}

}
