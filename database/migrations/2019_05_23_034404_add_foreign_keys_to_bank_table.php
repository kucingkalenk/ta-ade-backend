<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBankTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bank', function(Blueprint $table)
		{
			$table->foreign('level', 'bank_fk0')->references('id')->on('bank_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('type', 'bank_fk1')->references('id')->on('bank_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('schema', 'bank_fk2')->references('id')->on('bank_schemas')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bank', function(Blueprint $table)
		{
			$table->dropForeign('bank_fk0');
			$table->dropForeign('bank_fk1');
			$table->dropForeign('bank_fk2');
		});
	}

}
