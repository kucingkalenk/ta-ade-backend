<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBankTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bank', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('title', 65535);
			$table->text('media', 65535)->nullable();
			$table->integer('level')->index('bank_fk0');
			$table->integer('type')->index('bank_fk1');
			$table->integer('schema')->nullable()->index('bank_fk2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bank');
	}

}
