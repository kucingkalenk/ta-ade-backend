<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('questions', function(Blueprint $table)
		{
			$table->foreign('level', 'questions_fk0')->references('id')->on('levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('exam', 'questions_fk1')->references('id')->on('exams')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('type', 'questions_fk2')->references('id')->on('types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('questions', function(Blueprint $table)
		{
			$table->dropForeign('questions_fk0');
			$table->dropForeign('questions_fk1');
			$table->dropForeign('questions_fk2');
		});
	}

}
