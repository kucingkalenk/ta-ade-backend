<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('results', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user')->index('results_fk0');
			$table->integer('exam')->index('results_fk1');
			$table->integer('question_bank')->index('results_fk2');
			$table->integer('answer')->nullable()->index('results_fk3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('results');
	}

}
