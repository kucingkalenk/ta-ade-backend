<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
     * Seed the application's database.
     *
     * @return void
     */
  public function run()
  {
    // $this->call(UsersTableSeeder::class);

    // user
    $roles = ['admin','user','staff','master'];
    foreach ($roles as $i) {
      DB::table('user_roles')->insert([
        'role' => $i
      ]);
    }
    $profile = ['none'];
    foreach ($profile as $i) {
      DB::table('user_profiles')->insert([
        'profile' => $i
      ]);
    }

    $users = [
      [
        'nomor_induk' => 'head',
        'nama' => 'Master',
        'password' => bcrypt('head'),
        'role' => 4,
        'profile' => 1,
        'active' => true
      ],
      [
        'nomor_induk' => '1461505237',
        'nama' => 'Adeardo Dora Harnanda',
        'password' => bcrypt('1461505237'),
        'role' => 2,
        'profile' => 1,
        'active' => true
      ]
    ];

    foreach ($users as $i) {
      DB::table('users')->insert([
        'nomor_induk' => $i['nomor_induk'],
        'nama' => $i['nama'],
        'password' => $i['password'],
        'role' => $i['role'],
        'profile' => $i['profile'],
        'active' => $i['active']
      ]);
    }

    // exam
    $group = ['none'];
    foreach ($group as $i) {
      DB::table('exam_groups')->insert([
        'group' => $i
      ]);
    }

    // bank
    $schema = ['none', 'matematika', 'bahasa inggris', 'fisika', 'kimia', 'sejarah', 'sosiologi', 'ekonomi', 'ppkn'];
    foreach ($schema as $i) {
      DB::table('bank_schemas')->insert([
        'schema' => $i
      ]);
    }
    $type = ['none', 'us', 'unbk'];
    foreach ($type as $i) {
      DB::table('bank_types')->insert([
        'type' => $i
      ]);
    }
  }
}
