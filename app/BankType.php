<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankType extends Model
{
  protected $table = 'bank_types';
  protected $guarded = ['id'];
  public $timestamps = false;
}
