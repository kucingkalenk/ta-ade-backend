<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamGroup extends Model 
{

    protected $table = 'exam_groups';
    public $timestamps = false;
    protected $guarded = array('id');

}