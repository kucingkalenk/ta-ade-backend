<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model 
{

  protected $table = 'exams';
  public $timestamps = false;
  protected $guarded = array('id');

  public function group_relation()
  {
    return $this->belongsTo('App\Group', 'group');
  }

}