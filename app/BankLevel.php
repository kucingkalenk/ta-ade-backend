<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankLevel extends Model
{
  protected $table = 'bank_levels';
  protected $guarded = ['id'];
  public $timestamps = false;
}
