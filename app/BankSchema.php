<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankSchema extends Model
{
  protected $table = 'bank_schemas';
  protected $guarded = ['id'];
  public $timestamps = false;
}
