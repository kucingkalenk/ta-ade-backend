<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Choice;
use App\Exam;

class QuestionController extends Controller 
{

  public function index(Question $question)
  {
    return response()->json($question->all());
  }

  public function store(Request $request, Question $question)
  {
    if ($request->hasFile('media')) {
      $path = $request->media->storeAs('images', 'filename.jpg', 'media');
      $request['media'] = $path;
    }
    else {
      $request['media'] = '';
    }
    $question = $question->create($request->only(['title','media','score','exam']));
    $response = [
      'question_id' => $question->id,
      'status' => 'Sukses!'
    ];
    return response()->json($response);
  }

  public function show(Question $question)
  {
    return response()->json($question);
  }

  public function update(Question $question)
  {
    return response()->json($question);
  }

  public function destroy($id)
  {
    
  }

  public function import(Request $request)
  {
    
  }

  public function getByExam(Request $request, Question $question)
  {
    $datas = $question->where('exam', $request->get('exam'))->get();
    return response()->json($datas->toArray());
  }
  
}

?>