<?php

namespace App\Http\Controllers;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transformers\ViewUserTransformer;
use App\User;
use App\Akun;

class AuthController extends Controller
{

  public function __construct()
	{
    $this->middleware('auth:api', ['except' => ['login','register']]);
  }

  public function register(Request $request)
  {
    $user = User::create([
      'nomor_induk' => $request->nomor_induk,
      'nama' => $request->nama,
      'password' => bcrypt($request->nomor_induk),
      'role' => 1,
      'profile' => 1
    ]);

    $token = auth()->login($user);

    return $this->respondWithToken($token,$user);
  }

  public function login(Request $request)
  {
    $credentials = $request->only(['nomor_induk', 'password']);

    if (!$token = auth()->attempt($credentials)) {
      return response()->json(['message' => 'User not found.'], 401);
    }

    $user = User::find($request->user()->id);
    return $this->respondWithToken($token,$user);
  }

  public function me()
  {
    try {
      if (! $user = JWTAuth::parseToken()->authenticate()) {
        return response()->json(['user_not_found'], 404);
      }
    }
    catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      return response()->json(['token_expired'], $e->getStatusCode());
    }
    catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      return response()->json(['token_invalid'], $e->getStatusCode());
    }
    catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
      return response()->json(['token_absent'], $e->getStatusCode());
    }

    return fractal()
      ->item($user)
      ->transformWith(new ViewUserTransformer)
      ->toArray();
  }

  public function logout()
  {
    auth()->logout();

    return response()->json(['message' => 'Successfully logged out']);
  }

  public function refresh(Request $request)
  {
    $token = auth()->refresh();
    $user = User::find($request->user()->id);
    
    return $this->respondWithToken($token,$user);
  }

  protected function respondWithToken($token,$user = '', $meta = '')
  {
    return response()->json([
      'user' => $user,
      'access_token' => $token,
      'token_type' => 'bearer'
    ]);
  }

}
