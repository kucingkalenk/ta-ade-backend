<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transformers\ViewExamTransformer;
use App\Exam;

class ExamController extends Controller 
{

  public function index(Exam $exam)
  {
    return fractal()
    ->collection($exam->all())
    ->transformWith(new ViewExamTransformer)
    ->toArray();
  }

  public function store(Request $request, Exam $exam)
  {
    $token_validator = [1];
    while (count($token_validator) > 0) {
      $request['token'] = $this->getToken(5);
      $token_validator = $exam->where('token',$request['token'])->get();
    }
    // return response()->json($request->all());
    $exam = $exam->create($request->only(['title','time','date','group','token']));
    return response()->json('Sukses!');
  }

  public function show(Exam $exam)
  {
    return fractal()
    ->item($exam)
    ->transformWith(new ViewExamTransformer)
    ->toArray();
  }

  public function update($id)
  {
    
  }

  public function destroy(Exam $exam)
  {
    $exam->delete();
    return response()->json('Sukses!');
  }

  public function getToken($length){
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet);

    for ($i=0; $i < $length; $i++) {
      $token .= $codeAlphabet[random_int(0, $max-1)];
    }

    return $token;
  }
  
}

?>