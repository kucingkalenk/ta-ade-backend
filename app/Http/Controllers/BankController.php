<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transformers\ViewExamTransformer;
use App\Bank;
use App\BankType;
use App\BankSchema;
use App\BankLevel;
use App\Choice;

class BankController extends Controller
{

  public function index(Bank $bank)
  {
    return response()->json($bank->all());
  }

  public function store(Request $request, Bank $bank)
  {
    if ($request->hasFile('media')) {
      $path = $request->media->storeAs('images', 'filename.jpg', 'media');
      $request['media'] = $path;
    }
    else {
      $request['media'] = '';
    }
    $bank = $bank->create($request->only(['title','media','level','type','schema']));
    $response = [
      'status' => 'Sukses!'
    ];
    return response()->json($response);
  }

  public function show($id)
  {
    //
  }

  public function update(Request $request, $id)
  {
    //
  }

  public function destroy($id)
  {
    //
  }
}
