<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;

class GroupController extends Controller 
{

  public function index(Group $group)
  {
    return response()->json($group->all());
  }

  public function create()
  {
    
  }

  public function store(Request $request)
  {
    
  }

  public function show($id)
  {
    
  }

  public function edit($id)
  {
    
  }

  public function update($id)
  {
    
  }

  public function destroy($id)
  {
    
  }
  
}

?>