<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akun extends Model
{
	protected $table = 'akun';
	public $timestamps = false;

	public function user()
	{
		return $this->belongsTo('App\User','id_auth');
	}
}
