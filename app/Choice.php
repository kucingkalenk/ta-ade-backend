<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Choice extends Model 
{

    protected $table = 'choices';
    public $timestamps = false;
    protected $guarded = array('id');

    public function question()
    {
        return $this->hasOne('App\Question', 'question');
    }

}