<?php

namespace App\Helpers;

/* 
Aplikasi ini didedikasikan penuh kepada kesayanga saya Ratih Galuh Pradewi
Core helper untuk aplikasi iNeed

ditulis oleh Adeardo Dora Harnanda
Tgl 13 Mei 2019 (Ramadhan 11:06 WIB)
*/

class RGP {
  public static function tes($a) {
    return $a;
  }
  public static function fractal($data, $fractalClass, $isCollection = true, $serialize = true) {
    if ($isCollection == true) {
      if ($serialize == true)
        return fractal()
        ->collection($data)
        ->transformWith(new $fractalClass)
        ->serializeWith(new \Spatie\Fractalistic\ArraySerializer())
        ->toArray();
      else
        return fractal()
        ->collection($data)
        ->transformWith(new $fractalClass)
        ->toArray();
    }
    else {
      if ($serialize == true)
        return fractal()
        ->item($data)
        ->transformWith(new $fractalClass)
        ->serializeWith(new \Spatie\Fractalistic\ArraySerializer())
        ->toArray();
      else
        return fractal()
        ->item($data)
        ->transformWith(new $fractalClass)
        ->toArray();
    }
  }
}