<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model 
{

    protected $table = 'questions';
    public $timestamps = false;
    protected $guarded = array('id');

    public function exam()
    {
        return $this->hasOne('App\Exam', 'exam');
    }

}