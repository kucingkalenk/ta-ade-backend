<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model 
{

    protected $table = 'results';
    public $timestamps = true;
    protected $guarded = array('id');

    public function user()
    {
        return $this->hasOne('App\User', 'users');
    }

    public function exam()
    {
        return $this->hasOne('App\Exam', 'exam');
    }

    public function question()
    {
        return $this->hasOne('App\Question', 'question');
    }

    public function answer()
    {
        return $this->hasOne('App\Choice', 'answer');
    }

}