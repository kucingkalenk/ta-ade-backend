<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $table = 'users';

    protected $guarded = ['id'];
    
    // protected $fillable = [
    //     'username', 'email', 'hash', 'password',
    // ];

    // protected $hidden = [
    //     'password', 'token', 'hash'
    // ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function akun()
    {
        return $this->hasOne('App\Akun','id_auth');
    }
}
