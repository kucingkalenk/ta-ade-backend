<?php

namespace App\Transformers;

use App\Exam;
use App\Group;
use League\Fractal\TransformerAbstract;

class ViewExamTransformer extends TransformerAbstract
{
  public function transform(Exam $exam)
  {
    return [
      'id' => $exam->id,
      'title' => $exam->title,
      'date' => $exam->date,
      'time' => $exam->time,
      'token' => $exam->token,
      'group' => [
      	'id' => $exam->group,
      	'name' => $exam->group_relation->group
      ]
    ];
  }
}