<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class ViewUserTransformer extends TransformerAbstract
{
  public function transform(User $user)
  {
    return [
      'id' => $user->id,
      'nomor_induk' => $user->nomor_induk,
      'nama' => $user->nama_lengkap
    ];
  }

}